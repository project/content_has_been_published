Purpose

Answers the question: Has this node ever been published?

Installation

composer require drupal/content_has_been_published
